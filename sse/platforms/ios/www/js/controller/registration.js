var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'angular-websql',
        'ngCordova',
    ]);

app.controller('registrationController',['$scope','$http','$webSql','$cordovaFile', function($scope,$http,$webSql,$cordovaFile){

    $scope.newregistration = {};
    $scope.examcenters = [];

    $scope.showform=false;
    $scope.showaccept=true;
    $scope.applicationform = {};
    $scope.student={};

    $scope.publish=false;

    function getSettings(){
        $http.get('http://www.sse.scienceinstituteonline.in/getsettings/').
        success(function (data, status, headers, config) {
            if(data[0].application=='1')
                $scope.publish=true;
            else
                $scope.publish=false;

            //console.log(data);
        }).error(function (data, status, headers, config) {
            console.log(data);
        });
    }
    getSettings();

  function loadExamCentres(){

	$http.get('http://www.sse.scienceinstituteonline.in/getexamcentre/').
            success(function (data, status, headers, config) {
               $scope.examcenters=data;

                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
     	}loadExamCentres();

 	$scope.addRegistration = function () {
      if($scope.reject==false) {
            var db = $webSql.openDatabase('mydb', '1.0', 'SSE', 2 * 1024 * 1024); 
              db.selectAll("user").then(function(results) {
                  var appData=results.rows.item(0);
                  if(appData!=null)
                   {
                      var dateofbirth = $scope.date.year + "/" + $scope.date.month + "/" + $scope.date.day;
                      $http.get('http://www.sse.scienceinstituteonline.in/registerOnline',{params:{name:$scope.newregistration.name,
                        parentname:$scope.newregistration.parentname,
                        gender:$scope.newregistration.gender,
                        dateofbirth:dateofbirth,
                        parentmobile:$scope.newregistration.parentmobile,
                        landphone:$scope.newregistration.landphone,
                        schoolname:$scope.newregistration.schoolname,
                        address1:$scope.newregistration.address1,
                        address2:$scope.newregistration.address2,
                        district:$scope.newregistration.district,
                        pin:$scope.newregistration.pin ,
                        email:$scope.newregistration.email,
                        knownabout:$scope.newregistration.knownabout,
                        modeofpayment:$scope.newregistration.modeofpayment,
                        boardofexam:$scope.newregistration.boardofexam,
                        medium:$scope.newregistration.medium,
                        exam_centres_id1:$scope.newregistration.exam_centres_id1,
                        exam_centres_id2:$scope.newregistration.exam_centres_id2,
                        through:'Android App',
                        app_id:appData.id}}).
                      success(function (data, status, headers, config) {
                        $scope.newregistration={};
                        $scope.completed=true;
                        $scope.showform=false;
                        $scope.showaccept=false;
                        $scope.student = {};
                        $scope.applicationform = data;
                          console.log(data);
                      }).error(function (data, status, headers, config) {
                          console.log(data);
                      });
                   }
               });
          
          }else{
            alert('You already registered with this Parent/Guardian Mobile Number.Try to change this Mobile Number: '+$scope.newregistration.parentmobile);
          }
    };

    $scope.accept=function(){
    	$scope.showform=true;
    	$scope.showaccept=false;
    }

    $scope.checkPhoneExsist=function(phone){
        $http.get('http://www.sse.scienceinstituteonline.in/checkexsist/',{params:{phone:phone}}).
            success(function (data, status, headers, config) {
                $scope.student=data;
                $scope.reject=true;
                if(data.OK=='Ready')
                    $scope.reject=false;
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

     $scope.appHeader={};
    function generatePDF() {
        $http.get('getApplicationHeader').
            success(function (data, status, headers, config) {
                $http.get('header/'+data.photo).
                    success(function (data, status, headers, config) {
                            $scope.appHeader = 'data:image/jpeg;base64,'+data;
                            $scope.docDefinition = {
                            pageSize:'A4',
                            content: [
                                {
                                    image:$scope.appHeader,//'sampleImage.jpg',
                                    width: 515,
                                    height: 130
                                },
                                {
                                    style: 'tableExample',
                                    table: {
                                        widths: [200, '*'],
                                        body: [
                                            ['1. Name of the Student (In Block letters):', $scope.applicationform.name],
                                            ['2. Gender:', $scope.applicationform.gender],
                                            ['3. Date of Birth:', $scope.applicationform.dateofbirth],
                                            ['4.Name of Parent/Guardian:',$scope.applicationform.parentname],
                                            ['5.Mobile No.of Parent/Gaurdian:',$scope.applicationform.parentmobile],
                                            ['6.Telephone No with STD Code:',$scope.applicationform.landphone],
                                            ['7.Name of school now studying for SSLC:',$scope.applicationform.schoolname],
                                            ['8.Address to which communication are to be sent:',$scope.applicationform.address1+','+$scope.applicationform.address2],
                                            ['9.District:',$scope.applicationform.district],
                                            ['10.Pin Code:',$scope.applicationform.pin],
                                            ['11.Email:',$scope.applicationform.email],
                                            ['12.Examination Centre Preferred:','Choice 1:'+$scope.applicationform.exam_centres1.place+' \n\n Choice 2: '+$scope.applicationform.exam_centres2.place],
                                            ['13.Board of Examination:',$scope.applicationform.boardofexam],
                                            ['14.Medium of Study:',$scope.applicationform.medium],
                                            ['15.Mode of remittance:',$scope.applicationform.modeofpayment],
                                            ['16.How did you come to know about Science Institute ?:',$scope.applicationform.knownabout],
                                        ]
                                    }

                                },
                                { text: 'I affirm that the details shown above are true.', style: 'subheader' },
                                {
                                    style: 'tableHeader',
                                    table: {
                                        widths: ['auto' , '500'],
                                        body: [
                                            ['\n\nPlace:\n\nDate:',{ text: '\n\nSignature of Student',  alignment: 'right' } ],
                                        ]
                                    },
                                    layout: 'noBorders'

                                },
                                { text: '\nCertify that the above said student is studying in 10th standard of this school', style: 'tableHeader', alignment: 'center' },
                                {
                                    style: 'tableHeader',
                                    table: {
                                        widths: ['auto' , '500'],
                                        body: [
                                            ['\n\nOffice Seal',{ text: '\n\nSignature of Principal/Head Master',  alignment: 'right' } ],
                                        ]
                                    },
                                    layout: 'noBorders'

                                },

                            ],
                            styles: {
                                header: {
                                    fontSize: 18,
                                    bold: true,
                                    margin: [5, 0, 0, 10]
                                },
                                subheader: {
                                    fontSize: 14,
                                    bold: true,
                                    margin: [0, 10, 0, 5],
                                    color: 'red',
                                    alignment:'center'
                                },
                                tableExample: {
                                    margin: [0, 5, 0, 15],
                                    color: 'black'
                                },
                                tableHeader: {
                                    bold: true,
                                    fontSize: 13,
                                    color: 'black'

                                }
                            },
                            defaultStyle: {
                                // alignment: 'justify'
                            }
                        };
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
            }).error(function (data, status, headers, config) {
                    console.log(data);
            });
        }


    $scope.downloadPdf = function() {
        generatePDF();
        //pdfMake.createPdf($scope.docDefinition).download('SseApplication.pdf');
        /*$cordovaFile.createFile(cordova.file.externalRootDirectory, pdfMake.createPdf($scope.docDefinition).download('SseApplication.pdf'), true)
              .then(function (success) {
                 alert('success');
              }, function (error) {
                alert('Fails');
              });*/
        pdfMake.createPdf($scope.docDefinition).getDataUrl(function(dataURL) {
            var data=dataURL.replace('data:application/pdf;base64,', '');
            var fileToSave= b64toBlob(data, 'application/pdf'); 
            $cordovaFile.writeFile(cordova.file.externalRootDirectory,$scope.applicationform.name,fileToSave, true)
              .then(function (success) {
                 alert('Application Download Completed. Check your Internal/External Storage..');
              }, function (error) {
                alert('Fails');
              });
        });

    };

    function b64toBlob(b64Data, contentType, sliceSize) {
            var input = b64Data.replace(/\s/g, ''),
                byteCharacters = atob(input),
                byteArrays = [],
                offset, slice, byteNumbers, i, byteArray, blob;

            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            for (offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                slice = byteCharacters.slice(offset, offset + sliceSize);

                byteNumbers = new Array(slice.length);
                for (i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            //Convert to blob. 
            try {
                blob = new Blob(byteArrays, { type: contentType });
            }
            catch (e) {
                // TypeError old chrome, FF and Android browser
                window.BlobBuilder = window.BlobBuilder ||
                                     window.WebKitBlobBuilder ||
                                     window.MozBlobBuilder ||
                                     window.MSBlobBuilder;
                if (e.name == 'TypeError' && window.BlobBuilder) {
                    var bb = new BlobBuilder();
                    for (offset = 0; offset < byteArrays.length; offset += 1) {
                        bb.append(byteArrays[offset].buffer);
                    }                    
                    blob = bb.getBlob(contentType);
                }
                else if (e.name == "InvalidStateError") {
                    blob = new Blob(byteArrays, {
                        type: contentType
                    });
                }
                else {
                    return null;
                }
            }

            return blob;
        }

}]);

app.controller('homeController', function($scope,$http,$webSql){
    $scope.news=[];
    var fetched_json = localStorage.getItem("news");
    $scope.news=JSON.parse(fetched_json);


    $scope.photolists=[];

    /* var fetched_json = localStorage.getItem("photos");
        $scope.photos=JSON.parse(fetched_json);*/
    
    $scope.loadGallery=function(){
        $scope.photolists=[];    
        $http.get('http://localhost:8000/getslider/').
            success(function (data, status, headers, config) {
                //console.log(data);
                if(data!=null){
                    $scope.db = $webSql.openDatabase('mydb', '1.0', 'SSE', 5 * 1024 * 1024); 
                    $scope.db.dropTable('sliders');
                    $scope.db.createTable('sliders', {"id":{"type": "INTEGER","null": "NOT NULL"},
                              "photo":{"type": "TEXT","null": "NOT NULL"}});

                    $scope.lists=data;
                    var length=$scope.lists.length;
                    for(i=0;i<length;i++){
                            $scope.index=i;
                            $http.get('http://localhost:8000/header/'+$scope.lists[i].photo).
                                success(function (data, status, headers, config) {
                                var photo={ photo: 'data:image/png;base64,'+data };
                                $scope.photolists.push(photo);
                               
                                $scope.db.insert('sliders', {"id": i , "photo": photo.photo}).then(function(results) {
                                });
                                                
                                //console.log($scope.photos);
                            }).error(function (data, status, headers, config) {
                                    console.log(data);
                            });
                    }

                  
             }else{
                    $scope.db = $webSql.openDatabase('mydb', '1.0', 'SSE', 5 * 1024 * 1024); 
                    $scope.db.selectAll("sliders").then(function(results) {
                         $scope.photolists = [];
                         for(var i=0; i < results.rows.length; i++){
                                    $scope.photolists.push(results.rows.item(i));
                         }
                    });
             }

                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
                $scope.db = $webSql.openDatabase('mydb', '1.0', 'SSE', 5 * 1024 * 1024); 
                    $scope.db.selectAll("sliders").then(function(results) {
                         $scope.photolists = [];
                         for(var i=0; i < results.rows.length; i++){
                                    $scope.photolists.push(results.rows.item(i));
                         }
                    });
            });
    };
    $scope.loadGallery();
});


app.controller('galleryController', function($scope,$http,$webSql){
    
    
    $scope.photos=[];

    /* var fetched_json = localStorage.getItem("photos");
        $scope.photos=JSON.parse(fetched_json);*/
    
    $scope.loadGallery=function(){
        $scope.photos=[];    
        $http.get('http://www.sse.scienceinstituteonline.in/getgallery/').
            success(function (data, status, headers, config) {
                //console.log(data);
                if(data!=null){
                    $scope.db = $webSql.openDatabase('mydb', '1.0', 'SSE', 5 * 1024 * 1024); 
                    $scope.db.dropTable('images');
                    $scope.db.createTable('images', {"id":{"type": "INTEGER","null": "NOT NULL"},
                              "src":{"type": "TEXT","null": "NOT NULL"},
                              "href": {"type": "TEXT","null": "NOT NULL"}});

                    $scope.lists=data;
                    var length=$scope.lists.length;
                    for(i=0;i<length;i++){
                            $scope.index=i;
                            $http.get('http://www.sse.scienceinstituteonline.in/header/'+$scope.lists[i].photo).
                                success(function (data, status, headers, config) {
                                var photo={title: 'photo '+(i+1),
                                           href:'http://www.sse.scienceinstituteonline.in/images/'+$scope.lists[$scope.index].photo, src: 'data:image/png;base64,'+data};
                                $scope.photos.push(photo);
                               
                                $scope.db.insert('images', {"id": i , "src": photo.src , 'href': photo.href}).then(function(results) {
                                });
                                                
                                //console.log(data);
                            }).error(function (data, status, headers, config) {
                                    console.log(data);
                            });
                    }

                  
             }else{
                    $scope.db = $webSql.openDatabase('mydb', '1.0', 'SSE', 5 * 1024 * 1024); 
                    $scope.db.selectAll("images").then(function(results) {
                         $scope.photos = [];
                         for(var i=0; i < results.rows.length; i++){
                                    $scope.photos.push(results.rows.item(i));
                         }
                    });
             }

                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
                $scope.db = $webSql.openDatabase('mydb', '1.0', 'SSE', 5 * 1024 * 1024); 
                    $scope.db.selectAll("images").then(function(results) {
                         $scope.photos = [];
                         for(var i=0; i < results.rows.length; i++){
                                    $scope.photos.push(results.rows.item(i));
                         }
                    });
            });
    };//$scope.loadGallery();


});


app.controller('onlineresultController', function($scope,$http){

    $scope.result = [];
    $scope.publish=true;
    $scope.errors={};
    $scope.showerror=false;
    $scope.hallticketno='';
    $scope.hideResult=true;
    
    $scope.loadSettings=function(){
        $http.get('http://www.sse.scienceinstituteonline.in/getsettings/').
            success(function (data, status, headers, config) {
                if(data[0].result=='1')
                    $scope.publish=true;
                else
                    $scope.publish=false;

                console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
          };$scope.loadSettings();

    $scope.checkAnother=function(){
    	$scope.result = [];
	    $scope.publish=false;
	    $scope.errors={};
	    $scope.showerror=false;
	    $scope.hallticketno='';
	    $scope.hideResult=true;
    }

    $scope.getResult=function(){
        $http.get('http://www.sse.scienceinstituteonline.in/getresult',{params:{hallticketno:$scope.hallticketno}}).
            success(function (data, status, headers, config) {
                $scope.result=data;
                $scope.errors={};
                $scope.showerror=false;
                $scope.hideResult=false;
                console.log(data);
            }).error(function (data, status, headers, config) {
                $scope.errors=data;
                $scope.showerror=true;
                console.log(data);
            });  
          
    }
});

app.controller('newsController', function($scope,$http){

    $scope.news=[];
    var fetched_json = localStorage.getItem("news");
    $scope.news=JSON.parse(fetched_json);

    $scope.getnews=function(){
            $http.get('http://www.sse.scienceinstituteonline.in/getnews/').
                success(function (data, status, headers, config) {
                  if(data!=null){
                    localStorage.setItem('news', JSON.stringify(data));
                    $scope.news=data;
                    //console.log(data);
                  }else{
                     var fetched_json = localStorage.getItem("news");
                     $scope.news=JSON.parse(fetched_json);
                  }
                }).error(function (data, status, headers, config) {
                    console.log(data);
                    var fetched_json = localStorage.getItem("news");
                     $scope.news=JSON.parse(fetched_json);
                  
                });
    };$scope.getnews();
   
});

app.controller('questionController', function($scope,$http){

    $scope.questions=[];
    var fetched_json = localStorage.getItem("questions");
    $scope.questions=JSON.parse(fetched_json);

    $scope.getquestion=function(){
       $http.get('http://www.sse.scienceinstituteonline.in/getquestion/').
              success(function (data, status, headers, config) {
                  if(data!=null){
                    localStorage.setItem('questions', JSON.stringify(data));
                    $scope.questions=data;
                    //console.log(data);
                  }else{
                     var fetched_json = localStorage.getItem("questions");
                     $scope.questions=JSON.parse(fetched_json);
                  }
                
              }).error(function (data, status, headers, config) {
                     var fetched_json = localStorage.getItem("questions");
                     $scope.questions=JSON.parse(fetched_json);
                    console.log(data);
              });
    };$scope.getquestion();

   
   
});

app.controller('statusController',['$scope','$http','$webSql','$cordovaFile', function($scope,$http,$webSql,$cordovaFile){

    $scope.students=[];
    $scope.applicationform={};
    var fetched_json = localStorage.getItem("status");
    $scope.students=JSON.parse(fetched_json);

    $scope.getStatus=function(){

          var db = $webSql.openDatabase('mydb', '1.0', 'SSE', 2 * 1024 * 1024); 
          db.selectAll("user").then(function(results) {
              var data=results.rows.item(0);
              if(data!=null)
               {
                        $http.get('http://www.sse.scienceinstituteonline.in/applist/',{params:{id:data.id}}).
                    success(function (data, status, headers, config) {
                        if(data!=null){
                          localStorage.setItem('status', JSON.stringify(data));
                          $scope.students=data;
                          console.log(data);
                        }else{
                           var fetched_json = localStorage.getItem("status");
                           $scope.students=JSON.parse(fetched_json);
                        }
                      
                    }).error(function (data, status, headers, config) {
                           var fetched_json = localStorage.getItem("status");
                           $scope.students=JSON.parse(fetched_json);
                          console.log(data);
                    });
               }
              //console.log(data.id);
          });
    
    };$scope.getStatus();

    $scope.appHeader={};
    function generatePDF() {
        $http.get('getApplicationHeader').
            success(function (data, status, headers, config) {
                $http.get('header/'+data.photo).
                    success(function (data, status, headers, config) {
                            $scope.appHeader = 'data:image/jpeg;base64,'+data;
                            $scope.docDefinition = {
                            pageSize:'A4',
                            content: [
                                {
                                    image:$scope.appHeader,//'sampleImage.jpg',
                                    width: 515,
                                    height: 130
                                },
                                {
                                    style: 'tableExample',
                                    table: {
                                        widths: [200, '*'],
                                        body: [
                                            ['1. Name of the Student (In Block letters):', $scope.applicationform.name],
                                            ['2. Gender:', $scope.applicationform.gender],
                                            ['3. Date of Birth:', $scope.applicationform.dateofbirth],
                                            ['4.Name of Parent/Guardian:',$scope.applicationform.parentname],
                                            ['5.Mobile No.of Parent/Gaurdian:',$scope.applicationform.parentmobile],
                                            ['6.Telephone No with STD Code:',$scope.applicationform.landphone],
                                            ['7.Name of school now studying for SSLC:',$scope.applicationform.schoolname],
                                            ['8.Address to which communication are to be sent:',$scope.applicationform.address1+','+$scope.applicationform.address2],
                                            ['9.District:',$scope.applicationform.district],
                                            ['10.Pin Code:',$scope.applicationform.pin],
                                            ['11.Email:',$scope.applicationform.email],
                                            ['12.Examination Centre Preferred:','Choice 1:'+$scope.applicationform.exam_centres1.place+' \n\n Choice 2: '+$scope.applicationform.exam_centres2.place],
                                            ['13.Board of Examination:',$scope.applicationform.boardofexam],
                                            ['14.Medium of Study:',$scope.applicationform.medium],
                                            ['15.Mode of remittance:',$scope.applicationform.modeofpayment],
                                            ['16.How did you come to know about Science Institute ?:',$scope.applicationform.knownabout],
                                        ]
                                    }

                                },
                                { text: 'I affirm that the details shown above are true.', style: 'subheader' },
                                {
                                    style: 'tableHeader',
                                    table: {
                                        widths: ['auto' , '500'],
                                        body: [
                                            ['\n\nPlace:\n\nDate:',{ text: '\n\nSignature of Student',  alignment: 'right' } ],
                                        ]
                                    },
                                    layout: 'noBorders'

                                },
                                { text: '\nCertify that the above said student is studying in 10th standard of this school', style: 'tableHeader', alignment: 'center' },
                                {
                                    style: 'tableHeader',
                                    table: {
                                        widths: ['auto' , '500'],
                                        body: [
                                            ['\n\nOffice Seal',{ text: '\n\nSignature of Principal/Head Master',  alignment: 'right' } ],
                                        ]
                                    },
                                    layout: 'noBorders'

                                },

                            ],
                            styles: {
                                header: {
                                    fontSize: 18,
                                    bold: true,
                                    margin: [5, 0, 0, 10]
                                },
                                subheader: {
                                    fontSize: 14,
                                    bold: true,
                                    margin: [0, 10, 0, 5],
                                    color: 'red',
                                    alignment:'center'
                                },
                                tableExample: {
                                    margin: [0, 5, 0, 15],
                                    color: 'black'
                                },
                                tableHeader: {
                                    bold: true,
                                    fontSize: 13,
                                    color: 'black'

                                }
                            },
                            defaultStyle: {
                                // alignment: 'justify'
                            }
                        };
                    }).error(function (data, status, headers, config) {
                         console.log(data);
                    });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        }


    $scope.download=function(student){
        $scope.applicationform=angular.copy(student);
        generatePDF();
        //generatePDF($scope.applicationform);
        /*$scope.docDefinition = {
            pageSize:'A4',
            content: [
                {
                    image:'sampleImage.jpg',
                    width: 515,
                    height: 130
                },
                {
                    style: 'tableExample',
                    table: {
                        widths: [200, '*'],
                        body: [
                            ['1. Name of the Student (In Block letters):',$scope.applicationform.name],
                            ['2. Gender:', $scope.applicationform.gender],
                            ['3. Date of Birth:', $scope.applicationform.dateofbirth],
                            ['4.Name of Parent/Guardian:',$scope.applicationform.parentname],
                            ['5.Mobile No.of Parent/Gaurdian:',$scope.applicationform.parentmobile],
                            ['6.Telephone No with STD Code:',$scope.applicationform.landphone],
                            ['7.Name of school now studying for SSLC:',$scope.applicationform.schoolname],
                            ['8.Address to which communication are to be sent:',$scope.applicationform.address1+','+$scope.applicationform.address2],
                            ['9.District:',$scope.applicationform.district],
                            ['10.Pin Code:',$scope.applicationform.pin],
                            ['11.Email:',$scope.applicationform.email],
                            ['12.Examination Centre Preferred:','Choice 1:'+$scope.applicationform.exam_centres1.place+' \n\n Choice 2: '+$scope.applicationform.exam_centres2.place],
                            ['13.Board of Examination:',$scope.applicationform.boardofexam],
                            ['14.Medium of Study:',$scope.applicationform.medium],
                            ['15.Mode of remittance:',$scope.applicationform.modeofpayment],
                            ['16.How did you come to know about Science Institute ?:',$scope.applicationform.knownabout],
                        ]
                    }

                },
                { text: 'I affirm that the details shown above are true.', style: 'subheader' },
                {
                    style: 'tableHeader',
                    table: {
                        widths: ['auto' , '500'],
                        body: [
                            ['\n\nPlace:\n\nDate:',{ text: '\n\nSignature of Student',  alignment: 'right' } ],
                        ]
                    },
                    layout: 'noBorders'

                },
                { text: '\nCertify that the above said student is studying in 10th standard of this school', style: 'tableHeader', alignment: 'center' },
                {
                    style: 'tableHeader',
                    table: {
                        widths: ['auto' , '500'],
                        body: [
                            ['\n\nOffice Seal',{ text: '\n\nSignature of Principal/Head Master',  alignment: 'right' } ],
                        ]
                    },
                    layout: 'noBorders'

                },

            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [5, 0, 0, 10]
                },
                subheader: {
                    fontSize: 14,
                    bold: true,
                    margin: [0, 10, 0, 5],
                    color: 'red',
                    alignment:'center'
                },
                tableExample: {
                    margin: [0, 5, 0, 15],
                    color: 'black'
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'

                }
            },
            defaultStyle: {
                
            }
        };*/
        //console.log($scope.applicationform.name);
        //pdfMake.createPdf($scope.docDefinition).download();
        pdfMake.createPdf($scope.docDefinition).getDataUrl(function(dataURL) {
            var data=dataURL.replace('data:application/pdf;base64,', '');
            var fileToSave= b64toBlob(data, 'application/pdf'); 
            $cordovaFile.writeFile(cordova.file.externalRootDirectory,$scope.applicationform.name,fileToSave, true)
              .then(function (success) {
                 alert('Application Download Completed. Check your Internal/External Storage..');
              }, function (error) {
                alert('Fails');
              });
        });
    };

    //Helper function that converts base64 to blob
function b64toBlob(b64Data, contentType, sliceSize) {
    var input = b64Data.replace(/\s/g, ''),
        byteCharacters = atob(input),
        byteArrays = [],
        offset, slice, byteNumbers, i, byteArray, blob;

    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    for (offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        slice = byteCharacters.slice(offset, offset + sliceSize);

        byteNumbers = new Array(slice.length);
        for (i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    //Convert to blob. 
    try {
        blob = new Blob(byteArrays, { type: contentType });
    }
    catch (e) {
        // TypeError old chrome, FF and Android browser
        window.BlobBuilder = window.BlobBuilder ||
                             window.WebKitBlobBuilder ||
                             window.MozBlobBuilder ||
                             window.MSBlobBuilder;
        if (e.name == 'TypeError' && window.BlobBuilder) {
            var bb = new BlobBuilder();
            for (offset = 0; offset < byteArrays.length; offset += 1) {
                bb.append(byteArrays[offset].buffer);
            }                    
            blob = bb.getBlob(contentType);
        }
        else if (e.name == "InvalidStateError") {
            blob = new Blob(byteArrays, {
                type: contentType
            });
        }
        else {
            return null;
        }
    }

    return blob;
};
   
}]);

app.controller('notificationController',['$scope','$http', function($scope,$http){

    $scope.notifications=[];
    var fetched_json = localStorage.getItem("notification");
    $scope.notifications=JSON.parse(fetched_json);

    $scope.getNotification=function(){
       $http.get('http://www.sse.scienceinstituteonline.in/getnotification/').
              success(function (data, status, headers, config) {
                 if(data!=null){
                    localStorage.setItem('notification', JSON.stringify(data));
                    $scope.notifications=data;
                    console.log(data);
                  }else{
                     var fetched_json = localStorage.getItem("notification");
                     $scope.notifications=JSON.parse(fetched_json);
                  }
                  
                  //console.log(data);
              }).error(function (data, status, headers, config) {
                  var fetched_json = localStorage.getItem("notification");
                  $scope.notifications=JSON.parse(fetched_json);
                  console.log(data);
              });
    };$scope.getNotification();

}]);


app.controller('loginController',['$scope','$http','$webSql','$window', function($scope,$http,$webSql,$window){

    $scope.checkLogin=function(){
          var db = $webSql.openDatabase('mydb', '1.0', 'SSE', 2 * 1024 * 1024); 
          db.selectAll("user").then(function(results) {
              var data=results.rows.item(0);
              if(data!=null)
                $window.location.href='home.html';
              console.log(data.id);
          });
    };$scope.checkLogin();

    $scope.getLogin=function(){
            $http.get('http://www.sse.scienceinstituteonline.in/setapplogin/',{params:{name:$scope.name,phone:$scope.phone}}).
              success(function (data, status, headers, config) {
                  var db = $webSql.openDatabase('mydb', '1.0', 'SSE', 2 * 1024 * 1024); 
                  db.createTable('user', {"id":{"type": "INTEGER","null": "NOT NULL", "primary": true},
                              "name":{"type": "TEXT","null": "NOT NULL"},
                              "phone": {"type": "TEXT","null": "NOT NULL"}});

                  db.insert('user', {"id": data.id , "name": data.name , 'phone': data.phone}).then(function(results) {
                          console.log(results.insertId);
                          $window.location.href='home.html';
                  });

                    var noti={
                        app_id:'57540484-e494-4286-957e-9b6b2b8c6a11',
                        identifier:'ce777617da7f548fe7a9ab6febb56cf39fba6d382000c0395666288d961ee567',
                        language:'en',
                        timezone:'-28800',
                        game_version:'1.0',
                        device_os:'4.2.2',
                        device_type:'1',
                        device_model:'Android 4',
                        tags:{a:'1',foo:'bar'}
                    };
                   $http({
                        method: 'POST',
                        url: 'https://onesignal.com/api/v1/players',
                        data: noti,
                        headers: {'Content-Type': 'application/json'}
                    });

                  console.log(data);
              }).error(function (data, status, headers, config) {
                  console.log(data);
              });

             
      };

}]);

app.controller('onlinehallticketController', function ($scope, $http) {

    $scope.students=[];
    $scope.student={};
    $scope.showdetails=false;
    $scope.publish=false;
    $scope.date = [];

    $scope.getSettings=function(){
          $scope.showdetails=false;
          $http.get('http://www.sse.scienceinstituteonline.in/getsettings/').
              success(function (data, status, headers, config) {
                  if(data[0].hallticket=='1')
                      $scope.publish=true;
                  else
                      $scope.publish=false;

                  //console.log(data);
              }).error(function (data, status, headers, config) {
                  console.log(data);
              });

          $http.get('http://www.sse.scienceinstituteonline.in/getapplication',{params:{'status':'Approved'}})
              .then(function(response){
                  $scope.students=response.data;
                  console.log(response.data);
              });
      };


    $scope.getDetails=function(student){
        $scope.student=student;
        $scope.showdetails=true;
    };

    $scope.getHallTicket=function(){
        if($scope.date.year==null || $scope.date.month==null || $scope.date.day==null) {
            alert('Invalid Date of Birth');
            return 0;
        }
        var dateofbirth = $scope.date.year + "/" + $scope.date.month + "/" + $scope.date.day;
//http://ssedemo.psybotechnologies.com
        $scope.hallticket={};
        $http.get('http://www.sse.scienceinstituteonline.in/gethallticket',{params:{'date':dateofbirth,'id':$scope.student.id,'exam_centres_id':$scope.student.exam_centres_id}})
            .success(function (data, status, headers, config) {
                $scope.hallticket=data;
                generatePDF();
                $scope.error='';
                console.log($scope.hallticket);
                //console.log(data);
            }).error(function (data, status, headers, config) {
                $scope.error=data.error;
                console.log(data);
            });

    };

    $scope.appHeader={};
    function generatePDF() {
        $http.get('getHallTicketHeader').
            success(function (data, status, headers, config) {
                            $http.get('header/'+data.photo).
                                success(function (data, status, headers, config) {
                                    $scope.appHeader = 'data:image/jpeg;base64,'+data;
                                    var docDefinition = {
                                        pageSize:'A4',
                                        content: [
                                            {
                                                image:$scope.appHeader,//'sampleImage.jpg',
                                                width: 510,
                                                height: 130
                                            },
                                            {
                                                style: 'tableExample',
                                                table: {
                                                    body: [
                                                        [{text:'HALL TICKET', colSpan: 2,style:'subheader', alignment: 'center'},{}],
                                                        [{text:'Roll No',alignment:'center'},{text:$scope.student.hallticketno,alignment:'center'}],

                                                    ]
                                                }

                                            },
                                            {
                                                style: 'tableExample2',
                                                table: {
                                                    widths: [200, '*'],
                                                    body: [
                                                        ['Name of Student',{text:$scope.student.name,bold: true,fontSize: 16}],
                                                        ['Date & Time',{text:$scope.hallticket.datetime,bold: true,fontSize: 16}],
                                                        ['Examination Centre',{text:$scope.hallticket.exam_center.name+','+$scope.hallticket.exam_center.address+', Phone: '+$scope.hallticket.exam_center.phone,bold: true,fontSize: 16}],
                                                    ]
                                                }

                                            },
                                            {
                                                style: 'tableExample2',
                                                table: {
                                                    widths: [200, '*'],
                                                    body: [
                                                        ['Type of Exam',{text:$scope.hallticket.typeofexam}],
                                                        ['Subjects',{text:$scope.hallticket.subject}],
                                                        ['Syllabus',{text:$scope.hallticket.syllabus}],
                                                        ['Medium of Question Paper',{text:$scope.hallticket.medium}],
                                                        ['Website',{text:$scope.hallticket.website}],
                                                        ['Email',{text:$scope.hallticket.email}],
                                                    ]
                                                }

                                            },
                                            {
                                                style: 'tableExample2',
                                                table: {
                                                    widths: [200, '*'],
                                                    body: [
                                                        ['Place: Manjeri\n Date:'+new Date('dd-MM-yyyy'),{text:'Director',alignment:'right',fontSize: 15}],
                                                        [{text:'NB: Students should bring the Hall Ticket of SSLC Examination/School Identity Card with photograph/ Library card with photograph/ a certificate signed by the Head Master or Principal of the school in which the student is studying along with the hall ticket of scholarship examination issued by Science Institute',colSpan: 2,alignment:'justify'},{}],
                                                    ]
                                                },
                                                layout: 'noBorders'

                                            },
                                            { text:$scope.student.name+','+$scope.student.address1+','+$scope.student.address2+','+$scope.student.district+',Pin:'+ $scope.student.pin, fontSize: 14, bold: true, margin: [0, 20, 0, 8] },

                                        ],

                                        styles: {
                                            header: {
                                                fontSize: 18,
                                                bold: true,
                                                margin: [5, 0, 0, 10]
                                            },
                                            subheader: {
                                                fontSize: 24,
                                                bold: true,
                                                color: 'red',
                                                alignment:'center'
                                            },
                                            tableExample: {
                                                margin: [190, 25, 0, 15],
                                                color: 'black',
                                                fontSize: 14,
                                                bold: true
                                            },
                                            tableExample2: {
                                                margin: [10, 15, 0, 15],
                                                color: 'black',
                                                fontSize: 14
                                                //bold: true,
                                            },
                                            tableHeader: {
                                                bold: true,
                                                fontSize: 13,
                                                color: 'black'

                                            }
                                        },
                                        defaultStyle: {
                                            // alignment: 'justify'
                                        }
                                    };

                    //pdfMake.createPdf(docDefinition).download();
                    pdfMake.createPdf(docDefinition).getDataUrl(function(dataURL) {
                        var data=dataURL.replace('data:application/pdf;base64,', '');
                        var fileToSave= b64toBlob(data, 'application/pdf'); 
                        $cordovaFile.writeFile(cordova.file.externalRootDirectory,$scope.student.name+'_HallTicket',fileToSave, true)
                          .then(function (success) {
                             alert('Hall Ticket Download Completed. Check your Internal/External Storage..');
                          }, function (error) {
                            alert('Fails');
                          });
                    });
                    }).error(function (data, status, headers, config) {
                        console.log(data);
                    });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }


    function b64toBlob(b64Data, contentType, sliceSize) {
            var input = b64Data.replace(/\s/g, ''),
                byteCharacters = atob(input),
                byteArrays = [],
                offset, slice, byteNumbers, i, byteArray, blob;

            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            for (offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                slice = byteCharacters.slice(offset, offset + sliceSize);

                byteNumbers = new Array(slice.length);
                for (i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            //Convert to blob. 
            try {
                blob = new Blob(byteArrays, { type: contentType });
            }
            catch (e) {
                // TypeError old chrome, FF and Android browser
                window.BlobBuilder = window.BlobBuilder ||
                                     window.WebKitBlobBuilder ||
                                     window.MozBlobBuilder ||
                                     window.MSBlobBuilder;
                if (e.name == 'TypeError' && window.BlobBuilder) {
                    var bb = new BlobBuilder();
                    for (offset = 0; offset < byteArrays.length; offset += 1) {
                        bb.append(byteArrays[offset].buffer);
                    }                    
                    blob = bb.getBlob(contentType);
                }
                else if (e.name == "InvalidStateError") {
                    blob = new Blob(byteArrays, {
                        type: contentType
                    });
                }
                else {
                    return null;
                }
            }

            return blob;
        }

});



app.directive('dateField', function($filter) {
  return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModelController) {
           ngModelController.$parsers.push(function(data) {
              //View -> Model
              var date = Date.parseExact(data,'yyyy-MM-dd');
              ngModelController.$setValidity('date', date!=null);
              return date;
           });
           ngModelController.$formatters.push(function(data) {
              //Model -> View
              return $filter('date')(data, "yyyy-MM-dd");
           });    
       }
    }
});

app.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });



app.controller('testController',['$scope','$http','$webSql', function($scope,$http,$webSql){

    $scope.students=[];
    $scope.applicationform={};
    


    $scope.test=function(){
        $scope.applicationform={"id":1,"name":"Dr. Blake Lemke","registerno":"","parentname":"Dr. Kevin O'Keefe","gender":"Male","dateofbirth":"1999-09-14","parentmobile":"+1-580-501-5729","landphone":"(259) 699-0918 x9902","schoolname":"Jacobson, Casper and Russel","address1":"654 Harber Wall Suite 988\nLake Davinport, ME 98761-3509","address2":"911 Goodwin Bridge\nNew Sethport, FL 35461-6839","district":"Kingmouth","pin":"88468","email":"emilia33@cummings.com","knownabout":"Quam id facere necessitatibus fugiat aspernatur.","modeofpayment":"Direct","boardofexam":"CBSE","medium":"Other","status":"Approved","through":"Online","hallticketno":"","dateofexam":"2016-04-07","exam_centres_id":"7","exam_centres_id1":"1","exam_centres_id2":"9","download":"0","school_lists_id":null,"app_id":"","created_at":"2016-08-27 11:45:50","updated_at":"2016-08-27 11:45:50","exam_centres1":{"id":1,"name":"Dr. Nikita Fisher","place":"East Princebury","address":"25797 Genoveva Estate\nPort Nathenmouth, MO 50692-0557","phone":"(650) 873-8364 x0850","landmark":"West Nikitachester","description":"Facere voluptatem similique harum ratione exercitationem consequatur eius culpa.","active":"1","created_at":"2016-08-27 11:45:49","updated_at":"2016-08-27 11:45:49"},"exam_centres2":{"id":9,"name":"Leone Dare","place":"West Dorthyhaven","address":"752 Bauch Isle\nNew Hunterland, VT 58356","phone":"383.962.3320 x271","landmark":"Port Jamey","description":"Distinctio veniam voluptates voluptatem veniam eius quia cupiditate.","active":"1","created_at":"2016-08-27 11:45:49","updated_at":"2016-08-27 11:45:49"}};
        //generatePDF($scope.applicationform);
        $scope.docDefinition = {
            pageSize:'A4',
            content: [
               /* {
                    image:'sampleImage.jpg',
                    width: 515,
                    height: 130
                },*/
                {
                    style: 'tableExample',
                    table: {
                        widths: [200, '*'],
                        body: [
                            ['1. Name of the Student (In Block letters):',$scope.applicationform.name],
                            ['2. Gender:', $scope.applicationform.gender],
                            ['3. Date of Birth:', $scope.applicationform.dateofbirth],
                            ['4.Name of Parent/Guardian:',$scope.applicationform.parentname],
                            ['5.Mobile No.of Parent/Gaurdian:',$scope.applicationform.parentmobile],
                            ['6.Telephone No with STD Code:',$scope.applicationform.landphone],
                            ['7.Name of school now studying for SSLC:',$scope.applicationform.schoolname],
                            ['8.Address to which communication are to be sent:',$scope.applicationform.address1+','+$scope.applicationform.address2],
                            ['9.District:',$scope.applicationform.district],
                            ['10.Pin Code:',$scope.applicationform.pin],
                            ['11.Email:',$scope.applicationform.email],
                            ['12.Examination Centre Preferred:','Choice 1:'+$scope.applicationform.exam_centres1.place+' \n\n Choice 2: '+$scope.applicationform.exam_centres2.place],
                            ['13.Board of Examination:',$scope.applicationform.boardofexam],
                            ['14.Medium of Study:',$scope.applicationform.medium],
                            ['15.Mode of remittance:',$scope.applicationform.modeofpayment],
                            ['16.How did you come to know about Science Institute ?:',$scope.applicationform.knownabout],
                        ]
                    }

                },
                { text: 'I affirm that the details shown above are true.', style: 'subheader' },
                {
                    style: 'tableHeader',
                    table: {
                        widths: ['auto' , '500'],
                        body: [
                            ['\n\nPlace:\n\nDate:',{ text: '\n\nSignature of Student',  alignment: 'right' } ],
                        ]
                    },
                    layout: 'noBorders'

                },
                { text: '\nCertify that the above said student is studying in 10th standard of this school', style: 'tableHeader', alignment: 'center' },
                {
                    style: 'tableHeader',
                    table: {
                        widths: ['auto' , '500'],
                        body: [
                            ['\n\nOffice Seal',{ text: '\n\nSignature of Principal/Head Master',  alignment: 'right' } ],
                        ]
                    },
                    layout: 'noBorders'

                },

            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [5, 0, 0, 10]
                },
                subheader: {
                    fontSize: 14,
                    bold: true,
                    margin: [0, 10, 0, 5],
                    color: 'red',
                    alignment:'center'
                },
                tableExample: {
                    margin: [0, 5, 0, 15],
                    color: 'black'
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'

                }
            },
            defaultStyle: {
                // alignment: 'justify'
            }
        };
        //console.log($scope.applicationform.name);
        /*var data=pdfMake.createPdf($scope.docDefinition);
        console.log(data);*/
        var PDFdata;
            pdfMake.createPdf($scope.docDefinition).getDataUrl(function(dataURL) {
                var data=dataURL.replace('data:application/pdf;base64,', '');
                PDFdata = b64toBlob(data, 'application/pdf');  //
                console.log(PDFdata);
            });
            
    };

    function b64toBlob(b64Data, contentType, sliceSize) {
    var input = b64Data.replace(/\s/g, ''),
        byteCharacters = atob(input),
        byteArrays = [],
        offset, slice, byteNumbers, i, byteArray, blob;

    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    for (offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        slice = byteCharacters.slice(offset, offset + sliceSize);

        byteNumbers = new Array(slice.length);
        for (i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    //Convert to blob. 
    try {
        blob = new Blob(byteArrays, { type: contentType });
    }
    catch (e) {
        // TypeError old chrome, FF and Android browser
        window.BlobBuilder = window.BlobBuilder ||
                             window.WebKitBlobBuilder ||
                             window.MozBlobBuilder ||
                             window.MSBlobBuilder;
        if (e.name == 'TypeError' && window.BlobBuilder) {
            var bb = new BlobBuilder();
            for (offset = 0; offset < byteArrays.length; offset += 1) {
                bb.append(byteArrays[offset].buffer);
            }                    
            blob = bb.getBlob(contentType);
        }
        else if (e.name == "InvalidStateError") {
            blob = new Blob(byteArrays, {
                type: contentType
            });
        }
        else {
            return null;
        }
    }

    return blob;
};
   
}]);